#include <stdio.h>
#include <string.h>
#include <stdint.h>

#define IPS_EOF 0x454F46
#define ERROR_IF(cond, error) \
    do { if (cond) { fputs(error, stderr); return -1; } } while(0)

static const char header[] = {'P', 'A', 'T', 'C', 'H'};

uint32_t fread_be(FILE* file, uint32_t count) {
    uint32_t ret = 0;
    while (count--)
        ret |= (getc(file) & 0xFF) << (count * 8);
    return ret;
}

int main(int argc, char* argv[]) {
    ERROR_IF(argc > 2, "Usage: ips_parser [file]");
    FILE* patch = stdin;
    if (argc == 2)
        patch = fopen(argv[1], "rb");
    ERROR_IF(!patch, "Unable to open file");
    char buf[sizeof(header)];
    fread(buf, 1, sizeof(header), patch);
    ERROR_IF(memcmp(header, buf, sizeof(header)), "Not an ips patch file");
    for (uint32_t n = 0, addr; (addr = fread_be(patch, 3)) != IPS_EOF; n++) {
        printf("Record %i:\n", n);
        printf("\tAddress: %X\n", addr);
        uint16_t count = fread_be(patch, 2);
        if (count) {
            printf("\tCount:   %u\n\t", count);
            while (count--)
                printf("%.2X ", getc(patch));
            putchar('\n');
        } else {
            count = fread_be(patch, 2);
            printf("\tCount:   %u\n", count);
            printf("\tRepeat: %.2X\n", getc(patch));
        }
        putchar('\n');
    }
}
