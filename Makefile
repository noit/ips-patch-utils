# No default suffixes
.SUFFIXES:
# These targets are not files
.PHONY: all windows clean debug
# Compiler
CC = gcc
# Compiler flags
CFLAGS = -std=c11 -Wall -Wextra -Wno-unused-result -Werror -Isrc/include/ 
# Optimization flags
OPT_FLAGS = -O2 -flto
# Linker flags
LFLAGS =
# Executable
EXECUTABLE = bin/ips_utils 
# Build Directories
DIRECTORIES = bin/ obj/
# Source directory
SRC_DIR = src/
# Object directory
OBJ_DIR = obj/
# Object files
OBJS = main.o 
# Object full path
OBJS_FULL = $(addprefix $(OBJ_DIR), $(OBJS))

all: $(DIRECTORIES) $(EXECUTABLE) 

windows: CC=i686-w64-mingw32-gcc
windows: LFLAGS=-lmingw32 -mwindows
windows: EXECUTABLE=bin/ips_utils.exe
windows: all

clean:
	rm -rf $(DIRECTORIES)

debug: OPT_FLAGS=-ggdb
debug: all

$(DIRECTORIES):
	mkdir -p $@

$(EXECUTABLE): $(OBJS_FULL)
	$(CC) $^ $(LFLAGS) $(OPT_FLAGS) -o $(EXECUTABLE)

$(OBJ_DIR)%.o: $(SRC_DIR)%.c
	$(CC) $< $(CFLAGS) $(OPT_FLAGS) -c -o $@
